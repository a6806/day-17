import argparse
from typing import TextIO
from decimal import Decimal


class Probe:
    def __init__(self, velocity):
        x_velocity, y_velocity = velocity
        self.x_velocity = x_velocity
        self.y_velocity = y_velocity
        self.position = (0, 0)

    def step(self):
        x, y = self.position
        x += self.x_velocity
        y += self.y_velocity
        self.position = (x, y)

        if self.x_velocity > 0:
            self.x_velocity -= 1
        elif self.x_velocity < 0:
            self.x_velocity += 1

        self.y_velocity -= 1

    def get_position(self):
        return self.position


def is_on_target(velocity, target):
    target_x_min, target_x_max, target_y_min, target_y_max = target
    probe = Probe(velocity)
    pos_x, pos_y = probe.get_position()
    while pos_x <= target_x_max and pos_y >= target_y_min:
        probe.step()
        pos_x, pos_y = probe.get_position()
        if target_x_max >= pos_x >= target_x_min and target_y_max >= pos_y >= target_y_min:
            return True
    return False


def find_min_x_velocity(x_min, x_max):
    for x in range(x_min, x_max + 1):
        root = Decimal(2 * x) + Decimal('0.25')
        solution = root.sqrt() - Decimal('0.5')
        if solution == int(solution):
            return int(solution)


def find_height(y_velocity):
    n = y_velocity
    return int((n * (n + 1)) / 2)


def find_hits(target):
    target_x_min, target_x_max, target_y_min, target_y_max = target
    hits = []
    min_x_velocity = find_min_x_velocity(target_x_min, target_x_max)
    max_x_velocity = target_x_max
    for x_velocity in range(min_x_velocity, max_x_velocity + 1):
        y_velocity = -100
        while y_velocity < 100:
            hit = is_on_target((x_velocity, y_velocity), target)
            if hit:
                hits.append((x_velocity, y_velocity))
            y_velocity += 1

    return hits


def parse_input(text):
    _, target = text.split(': ')
    x, y = target.split(', ')
    x_min, x_max = x.split('..')
    x_min = int(x_min[2:])
    x_max = int(x_max)
    y_min, y_max = y.split('..')
    y_min = int(y_min[2:])
    y_max = int(y_max)
    target = (x_min, x_max, y_min, y_max)
    return target


def part_1(input_file: TextIO):
    target = parse_input(input_file.read().strip())
    return max([find_height(x[1]) for x in find_hits(target)])


def part_2(input_file: TextIO):
    target = parse_input(input_file.read().strip())
    return len(find_hits(target))


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 17')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
